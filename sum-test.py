import unittest
import Calc
class TestStringMethods(unittest.TestCase):

    def test_sum(self):
        self.assertEqual(Calc.Calc().sum(4,5), 9)

    def test_sum_with_zero(self):
        self.assertEqual(Calc.Calc().sum(4,0), 4)
    def test_sub(self):
        self.assertEqual(Calc.Calc().sub(4,0),4)
    def test_multiply(self):
        self.assertEqual(Calc.Calc().multiply(4,2),8)
    def test_divByZero(self):
        self.assertEqual(Calc.Calc().division(4,0),0)
    def test_modulo(self):
        self.assertEqual(Calc.Calc().modulo(4,2),0)
if __name__ == '__main__':
    unittest.main()